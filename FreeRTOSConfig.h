#define configGENERATE_RUN_TIME_STATS 1
#define configUSE_TRACE_FACILITY 1
#define configUSE_STATS_FORMATTING_FUNCTIONS 1
#define portCONFIGURE_TIMER_FOR_RUN_TIME_STATS() {}
#define portGET_RUN_TIME_COUNTER_VALUE() (RTC.COUNTER)


#define PUB_MSG_LEN 128

#define TRACE

#ifdef TRACE
#define GPIO_MAIN           12
#define GPIO_STATION        13
#define GPIO_MQTT           0
#define GPIO_WIFI           5
#define TRACE_ENABLE(GPIO)  gpio_enable(GPIO, GPIO_OUTPUT)
#define TRACE_UP(GPIO)      gpio_write(GPIO, 1)
#define TRACE_DOWN(GPIO)    gpio_write(GPIO, 0)
#else
#define GPIO_MAIN           12
#define GPIO_STATION        13
#define GPIO_MQTT           14
#define GPIO_WIFI           5
#define TRACE_ENABLE(GPIO)  {}
#define TRACE_UP(GPIO)      {}
#define TRACE_DOWN(GPIO)    {}
#endif

/* Use the defaults for everything else */
#include_next<FreeRTOSConfig.h>
