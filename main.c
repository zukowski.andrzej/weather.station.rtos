#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <queue.h>
#include <string.h>

#include <espressif/esp_common.h>
#include <espressif/esp_sta.h>
#include <espressif/esp_wifi.h>
#include <espressif/user_interface.h>

#include <ssid_config.h>
#include "esp/uart.h"

#include "wifi.h"
#include "station.h"
#include "mqtt.h"

SemaphoreHandle_t xWifiAlive;
QueueHandle_t     xPublishQueue;

void user_init(void)
{
    TRACE_ENABLE( GPIO_MAIN );
    TRACE_DOWN( GPIO_MAIN );

    uart_set_baud(0, 115200);
    printf("SDK version:%s\n", sdk_system_get_sdk_version());

    vSemaphoreCreateBinary(xWifiAlive);
    xPublishQueue = xQueueCreate(3, PUB_MSG_LEN);

    xTaskCreate(&wifi_task,    "wifi_task",    256, NULL, 2, NULL);
    xTaskCreate(&station_task, "station_task", 512, NULL, 3, NULL);
    xTaskCreate(&mqtt_task,    "mqtt_task",   2048, NULL, 4, NULL);

    TRACE_UP( GPIO_MAIN );
}
