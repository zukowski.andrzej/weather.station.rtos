#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <semphr.h>
#include <string.h>

#include "espressif/esp_common.h"

#include <paho_mqtt_c/MQTTESP8266.h>
#include <paho_mqtt_c/MQTTClient.h>

#include "mqtt.h"
#include "credentials.h"

#define MQTT_HOST  ("mqtt.thingspeak.com")
#define MQTT_PORT  1883
#define MQTT_TOPIC "channels/" THINGSPEAK_CHANNEL "/publish/" THINGSPEAK_API_KEY

#define MQTT_USER NULL
#define MQTT_PASS NULL
#define MQTT_CLIENT_ID "ESP"

#define MQTT_BUFSIZE 256

extern SemaphoreHandle_t xWifiAlive;
extern QueueHandle_t     xPublishQueue;

void mqtt_task(void *pvParameters)
{
    TRACE_ENABLE( GPIO_MQTT );

    mqtt_client_t client = mqtt_client_default;
    char    msg[PUB_MSG_LEN];
    int     ret;
    struct  mqtt_network network;
    uint8_t mqtt_buf[MQTT_BUFSIZE];
    uint8_t mqtt_readbuf[MQTT_BUFSIZE];
    mqtt_packet_connect_data_t data = mqtt_packet_connect_data_initializer;

    data.willFlag           = 0;
    data.MQTTVersion        = 3;
    data.clientID.cstring   = MQTT_CLIENT_ID;
    data.username.cstring   = MQTT_USER;
    data.password.cstring   = MQTT_PASS;
    data.keepAliveInterval  = 10;
    data.cleansession       = 1;

    xSemaphoreTake(xWifiAlive, 0);
    mqtt_network_new( &network );

    if( xQueueReceive(xPublishQueue, (void *)msg, portMAX_DELAY)  == pdTRUE) {
        mqtt_message_t message = {
            .qos        = MQTT_QOS0,
            .retained   = 0,
            .dup        = 0,
            .id         = 0,
            .payload    = msg,
            .payloadlen = strlen(msg),
        };

        //printf("XXX: %s\n", msg);
        TRACE_UP( GPIO_MQTT );
        xSemaphoreTake(xWifiAlive, portMAX_DELAY);
        TRACE_DOWN( GPIO_MQTT );

        if( !( ret = mqtt_network_connect(&network, MQTT_HOST, MQTT_PORT)) ) {
            mqtt_client_new
                ( &client
                , &network
                , 5000
                , mqtt_buf
                , MQTT_BUFSIZE
                , mqtt_readbuf
                , MQTT_BUFSIZE);

            if( !(ret = mqtt_connect(&client, &data)) ) {
                if( !(ret = mqtt_publish(&client, MQTT_TOPIC, &message)) != MQTT_SUCCESS ) {
                    printf("MQTT: Published\n");
                }
                else {
                    printf("error while publishing message: %d\n", ret );
                }
            }
            else {
                printf("Send MQTT connect ... error: %d\n\r", ret);
            }
        }
        else {
            printf("Connecting to MQTT server %s:%d ... error %d\n\r", MQTT_HOST, MQTT_PORT, ret);
        }
    }
    mqtt_network_disconnect(&network);

    TRACE_UP( GPIO_MQTT );
    sdk_system_deep_sleep(1000000 * 600);

    vTaskDelete(NULL);
}
