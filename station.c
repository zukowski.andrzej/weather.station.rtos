#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <string.h>

#include "espressif/esp_common.h"
#include "station.h"
#include "esplibs/libphy.h"

#include "i2c/i2c.h"
#include "bmp280/bmp280.h"

extern QueueHandle_t xPublishQueue;

const uint8_t i2c_bus = 0;
const uint8_t scl_pin = 14;
const uint8_t sda_pin = 2;

//static char pcBuffer[256];

void station_task(void *pvParameters)
{
    char msg[PUB_MSG_LEN];
    bmp280_params_t  params;
    float pressure, temperature, humidity, voltage;

    TRACE_ENABLE( GPIO_STATION );
    TRACE_DOWN( GPIO_STATION );

    i2c_init(i2c_bus, scl_pin, sda_pin, I2C_FREQ_400K);

    bmp280_init_default_params(&params);
    params.mode = BMP280_MODE_FORCED;

    bmp280_t bmp280_dev;
    bmp280_dev.i2c_dev.bus = i2c_bus;
    bmp280_dev.i2c_dev.addr = BMP280_I2C_ADDRESS_0;

    while (!bmp280_init(&bmp280_dev, &params)) {
        printf("BMP280 initialization failed\n");
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }

    bool bme280p = bmp280_dev.id == BME280_CHIP_ID;

    if (bmp280_force_measurement(&bmp280_dev)) {

        vTaskDelay(1000 / portTICK_PERIOD_MS);
        while (bmp280_is_measuring(&bmp280_dev)) {
            vTaskDelay(200 / portTICK_PERIOD_MS);
        };

        if (bmp280_read_float(&bmp280_dev, &temperature, &pressure, &humidity)) {
            voltage = sdk_readvdd33() / 1000.0;

            if (bme280p) {
                printf("Pressure: %.2f hPa, Temperature: %.2f C, Humidity: %.2f %%, Voltage: %.3f V\n"
                    , pressure/100
                    , temperature
                    , humidity
                    , voltage );
                snprintf(msg, PUB_MSG_LEN, "field1=%f&field2=%f&field3=%f&field4=%f"
                    , temperature
                    , pressure/100
                    , humidity
                    , voltage );
                //snprintf(msg, PUB_MSG_LEN, "field1=%f&field2=%f&field3=%f"
                //    , temperature
                //    , pressure/100
                //    , humidity );
            }
            else {
                printf("Pressure: %.2f hPa, Temperature: %.2f C, Voltage: %.3f V\n"
                    , pressure/100
                    , temperature
                    , voltage );
                snprintf(msg, PUB_MSG_LEN, "field1=%f&field2=%f&field4=%f"
                    , temperature
                    , pressure/100
                    , voltage );
            }
            //vTaskGetRunTimeStats( pcBuffer );
            //printf( pcBuffer );

            if (xQueueSend(xPublishQueue, (void *)msg, 0) == pdFALSE) {
                printf("Publish queue overflow.\r\n");
            }
        }
        else {
            printf("Temperature/pressure reading failed\n");
        }
    }
    else {
        printf("Failed initiating measurement\n");
    }

    TRACE_UP( GPIO_STATION );
    vTaskDelete( NULL);
}
