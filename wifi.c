#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <string.h>

#include <espressif/esp_common.h>
#include <espressif/user_interface.h>
#include <espressif/esp_sta.h>
#include <espressif/esp_wifi.h>

#include <ssid_config.h>

#include "wifi.h"

extern SemaphoreHandle_t xWifiAlive;

void wifi_task(void *pvParameters)
{
    struct sdk_station_config config = {
        .ssid      = WIFI_SSID,
        .password  = WIFI_PASS,
#ifdef WIFI_BSSID
        .bssid_set = 1,
        .bssid     = WIFI_BSSID
#endif
    };

    TRACE_ENABLE( GPIO_WIFI );
    TRACE_DOWN( GPIO_WIFI );

    sdk_wifi_set_opmode(STATION_MODE);

#if defined WIFI_IP && defined WIFI_NETMSK && defined WIFI_GW_IP
    struct ip_info info;
    memset(&info, 0x0, sizeof(info));
    info.ip.addr      = ipaddr_addr( WIFI_IP );
    info.netmask.addr = ipaddr_addr( WIFI_NETMSK );
    info.gw.addr      = ipaddr_addr( WIFI_GW_IP );

    sdk_wifi_station_dhcpc_stop();
    sdk_wifi_set_ip_info(STATION_IF, &info);
#endif

    sdk_wifi_station_set_config(&config);

    while(1)
    {
        uint8_t status  = 0;

        TRACE_DOWN( GPIO_WIFI );

        while ((status = sdk_wifi_station_get_connect_status()) != STATION_GOT_IP) {
            if( status == STATION_WRONG_PASSWORD ){
                printf("WiFi: wrong password\n\r");
                break;
            } else if( status == STATION_NO_AP_FOUND ) {
                printf("WiFi: AP not found\n\r");
                break;
            } else if( status == STATION_CONNECT_FAIL ) {
                printf("WiFi: connection failed\r\n");
                break;
            }
            vTaskDelay( 250 / portTICK_PERIOD_MS );
        }

        while ((status = sdk_wifi_station_get_connect_status()) == STATION_GOT_IP) {
            xSemaphoreGive( xWifiAlive );

            TRACE_UP( GPIO_WIFI );
            vTaskDelay( 1000 / portTICK_PERIOD_MS );
        }

        printf("WiFi: disconnected\n\r");
        sdk_wifi_station_disconnect();
    }
}
